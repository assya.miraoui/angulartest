# Test

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.8.

## Development server
Run `npm i` pour installer toutes les dépendances 

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

Run `ng generate module module-name --routing` to generate a new module.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).


## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## Déploiement
Install the Heroku CLI
tapez les commandes suivantes :
  `heroku login`
  `heroku git:clone -a myappitgate`
  `cd myappitgate`
  `git add .`
  `git commit -am "make it better`
   `git push heroku master`
 
## Architecture projet 
 MVVM : modele view - view modele
 


## les routes :
https://myappitgate.herokuapp.com/ page home 

https://myappitgate.herokuapp.com/detailProduit/:id ( page detail produit en cliquant sur le nom du produit vous trouvez les details)

https://myappitgate.herokuapp.com/listeProduit page liste des produit 



