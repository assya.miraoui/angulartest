import { Component, OnInit } from '@angular/core';
import {SvgIconRegistryService} from 'angular-svg-icon';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {

  public cardList: any;


  constructor() {

  }

  ngOnInit() {

this.cardList = [

  {title : 'Outdoors', lien : 'Outdoors Shop' , image: 'card3.jpg'},
  {title : 'Indoors', lien : 'Indoors Shop' , image: 'card4.jpg'},

];

  }
}



