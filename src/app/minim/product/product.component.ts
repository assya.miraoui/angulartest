import { Component, OnInit, HostListener } from '@angular/core';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { ProduitService } from '../../Shared/service/produit.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
  title = 'Popular';
    public Produits: any;


    constructor(private produitServ : ProduitService) {

    }

    ngOnInit() {
        this.produitServ.getAll().subscribe(res=>{

            this.Produits = res;
        })
    }
}
