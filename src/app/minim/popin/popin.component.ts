import { Component, OnInit } from '@angular/core';
import {NgxSmartModalService} from 'ngx-smart-modal';

@Component({
  selector: 'app-popin',
  templateUrl: './popin.component.html',
  styleUrls: ['./popin.component.css']
})
export class PopinComponent implements OnInit {

  constructor(public ngxSmartModalService: NgxSmartModalService,) { }

  ngOnInit() {
  }

}
