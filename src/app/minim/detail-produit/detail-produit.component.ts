import { Component, OnInit } from '@angular/core';
import {ProduitService} from '../../Shared/service/produit.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-detail-produit',
  templateUrl: './detail-produit.component.html',
  styleUrls: ['./detail-produit.component.css']
})
export class DetailProduitComponent implements OnInit {
prod : any ;
id : any ;
  constructor(private produitservice: ProduitService, private activeRoute: ActivatedRoute) {

    this.id = this.activeRoute.params['value']['id'];
  }

  ngOnInit() {

    this.produitservice.getByID(this.id).subscribe(res => {
      this.prod = res;
    });
  }

}
