import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MinimRoutingModule } from './minim-routing.module';
import { CardComponent } from './card/card.component';
import {CarouselComponent} from './carousel/carousel.component';
import {NewsLetterComponent} from './news-letter/news-letter.component';
import {CollectionsComponent} from './collections/collections.component';
import {ProductComponent} from './product/product.component';
import { MainComponent } from './main/main.component';
import {HeaderComponent} from '../theme/components/header/header.component';
import {NgxSmartModalModule} from 'ngx-smart-modal';
import {NgxPaginationModule} from 'ngx-pagination';
import {AngularSvgIconModule} from 'angular-svg-icon';
import {FooterComponent} from '../theme/components/footer/footer.component';
import { DetailProduitComponent } from './detail-produit/detail-produit.component';
import { ListeProduitsComponent } from './liste-produits/liste-produits.component';
import { PopinComponent } from './popin/popin.component';
import { CartComponent } from './cart/cart.component';


@NgModule({
  declarations: [
    CardComponent,
    MainComponent,
    CarouselComponent,
    NewsLetterComponent,
    CollectionsComponent,
    ProductComponent,
    HeaderComponent,
    FooterComponent,
    DetailProduitComponent,
    ListeProduitsComponent,
    PopinComponent,
    CartComponent
  ],
  imports: [
    CommonModule,
    MinimRoutingModule,
    NgxPaginationModule,
    AngularSvgIconModule,

    NgxSmartModalModule.forRoot()


  ]
})
export class MinimModule { }
