import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CardComponent} from './card/card.component';
import {CarouselComponent} from './carousel/carousel.component';
import {NewsLetterComponent} from './news-letter/news-letter.component';
import {CollectionsComponent} from './collections/collections.component';
import {MainComponent} from './main/main.component';
import {DetailProduitComponent} from './detail-produit/detail-produit.component';
import {ListeProduitsComponent} from './liste-produits/liste-produits.component';
import {CartComponent} from './cart/cart.component';


const routes: Routes = [

  {
    path: '',
    component: MainComponent
  },
  {
    path: 'card',
    component: CardComponent
  },
  {
    path: 'caroussel',
    component: CarouselComponent
  },
  {
    path: 'NewsLetter',
    component: NewsLetterComponent
  },
  {
    path: 'collection',
    component: CollectionsComponent
  },
  {
    path: 'detailProduit/:id',
    component: DetailProduitComponent
  },
  {
    path: 'listeProduit',
    component: ListeProduitsComponent
  },
   {
        path: 'cart',
        component: CartComponent
    },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MinimRoutingModule { }
