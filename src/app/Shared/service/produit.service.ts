import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProduitService {

  Url = 'https://app-4a240999-9341-4962-8060-96.herokuapp.com/'
  constructor(private http: HttpClient) { }

  getAll() {

    return this.http.get(this.Url + 'produit/');
  }


    getByID(id) {

        return this.http.get(this.Url + 'produit/' + id);
    }

}
