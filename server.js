const express = require('express');
const path = require('path');
const app = express();

// Serve static files....
app.use(express.static(__dirname + '/dist/test'));

// Send all requests to index.html

console.log(__dirname)
app.get('/*', function(req, res) {
    res.sendFile(path.join(__dirname + '/dist/test/index.html'));
});

// default Heroku PORT
app.listen(process.env.PORT || 3000);